/* the hello world server from nodejs */

var http = require('http');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World\n');
}).listen(process.env.PORT, process.env.IP);

console.log('Server running... ');


var connect = require('connect');
var app = connect();

//middleware goes here

//define
var helloWorld = function(req, res, next)
{
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello MEANies');
  next();
}
//associate/use
app.use('/normal', helloWorld);

//mounted
var helloFoo = function(req, res, next)
{
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello in Foo');
  next();
}
app.use('/foo', helloFoo);

app.listen(process.env.PORT);

console.log('server running');